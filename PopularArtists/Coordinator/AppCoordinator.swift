//
//  AppCoordinator.swift
//  PopularArtists
//
//  Created by Waheed Malik on 29/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class AppCoordinator : Coordinator {
    // MARK:- PRIVATE VARIABLES
    private var window: UIWindow
    private var topArtistsCoordinator : TopArtistsCoordinator?
    
    // MARK:- INITIALIZER
    init(window: UIWindow) {
        self.window = window
    }
    
    // MARK:- PUBLIC METHODS
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let rootNavController = storyboard.instantiateInitialViewController() as? UINavigationController {
            self.window.rootViewController = rootNavController
            self.showTopArtists(rootNavController: rootNavController)
        }
    }
    
    // MARK:- PRIVATE METHODS
    private func showTopArtists(rootNavController: UINavigationController) {
        topArtistsCoordinator = TopArtistsCoordinator(navController: rootNavController)
        topArtistsCoordinator?.start()
    }
}
