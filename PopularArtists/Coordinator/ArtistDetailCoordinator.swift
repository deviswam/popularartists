//
//  ArtistDetailCoordinator.swift
//  PopularArtists
//
//  Created by Waheed Malik on 01/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class ArtistDetailCoordinator : Coordinator {
    // MARK:- PRIVATE VARIABLES
    private var navigationController: UINavigationController
    private var selectedArtist: Artist
    
    // MARK:- INITIALIZER
    init(navController: UINavigationController, selectedArtist: Artist) {
        self.navigationController = navController
        self.selectedArtist = selectedArtist
    }
    
    // MARK:- PUBLIC METHODS
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let artistDetailVC = storyboard.instantiateViewController(withIdentifier: "ArtistDetailViewController") as? ArtistDetailViewController {
            let artistManager = SharedComponentsDir.artistManager
            let imageStore = SharedComponentsDir.imageStore
            let albumsCollectionViewDataSource = AlbumsCollectionViewDataSource()
            let albumsCollectionViewDelegate = AlbumsCollectionViewDelegate()
            let artistDetailViewModel = ArtistDetailViewModelImpl(selectedArtist: self.selectedArtist, artistManager: artistManager, imageStore: imageStore, viewDelegate: artistDetailVC)
            
            artistDetailVC.viewModel = artistDetailViewModel
            artistDetailVC.albumsCollectionViewDataSource = albumsCollectionViewDataSource
            artistDetailVC.albumsCollectionViewDelegate = albumsCollectionViewDelegate
            albumsCollectionViewDataSource.viewModel = artistDetailViewModel
            self.navigationController.pushViewController(artistDetailVC, animated: true)
        }
    }
}
