//
//  Coordinator.swift
//  PopularArtists
//
//  Created by Waheed Malik on 29/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

protocol Coordinator {
    func start()
}
