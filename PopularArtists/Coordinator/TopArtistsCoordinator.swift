//
//  TopArtistsCoordinator.swift
//  PopularArtists
//
//  Created by Waheed Malik on 29/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class TopArtistsCoordinator : Coordinator {
    // MARK:- PRIVATE VARIABLES
    private var navigationController: UINavigationController
    private var artistDetailCoordinator: ArtistDetailCoordinator?
    
    // MARK:- INITIALIZER
    init(navController: UINavigationController) {
        self.navigationController = navController
    }
    
    // MARK:- PUBLIC METHODS
    func start() {
        if let topArtistsVC = navigationController.topViewController as? TopArtistsViewController {
            let artistManager = SharedComponentsDir.artistManager
            let imageStore = SharedComponentsDir.imageStore
            let artistsCollectionViewDataSource = TopArtistsCollectionViewDataSource()
            let artistsCollectionViewDelegate = ArtistsCollectionViewDelegate()
            
            let topArtistsViewModel = TopArtistsViewModelImpl(artistManager: artistManager, imageStore: imageStore, viewDelegate: topArtistsVC, coordinatorDelegate: self)
            
            topArtistsVC.viewModel = topArtistsViewModel
            topArtistsVC.artistsCollectionViewDataSource = artistsCollectionViewDataSource
            artistsCollectionViewDataSource.viewModel = topArtistsViewModel
            topArtistsVC.artistsCollectionViewDelegate = artistsCollectionViewDelegate
            artistsCollectionViewDelegate.viewModel = topArtistsViewModel
        }
    }
}

extension TopArtistsCoordinator: TopArtistsViewModelCoordinatorDelegate {
    func didSelect(artist: Artist) {
        artistDetailCoordinator = ArtistDetailCoordinator(navController: self.navigationController, selectedArtist: artist)
        artistDetailCoordinator?.start()
    }
}
