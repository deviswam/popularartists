//
//  AlbumsCollectionViewDataSource.swift
//  PopularArtists
//
//  Created by Waheed Malik on 01/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class AlbumsCollectionViewDataSource : NSObject, UICollectionViewDataSource {
    var viewModel: ArtistDetailViewModel?
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let viewModel = viewModel {
            return viewModel.numberOfAlbums()
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlbumCollectionViewCell", for: indexPath)
        if let albumCell = cell as? AlbumCollectionViewCell, let albumVM = viewModel?.album(at: indexPath.item) {
            albumCell.configCell(with: albumVM)
            loadPhoto(withId: albumVM.album.id, photoUrl: albumVM.album.imageUrl, onCell: albumCell)
        }
        return cell
    }
    
    func loadPhoto(withId id: UUID, photoUrl: URL?, onCell cell:AlbumCollectionViewCell) {
        guard let photoUrl = photoUrl else { return }
        viewModel?.getPhoto(forId: id, withUrl: photoUrl, { (result: Result<UIImage>) in
            if case let Result.success(image) = result {
                cell.setAlbumImage(image: image)
            }
        })
    }
}
