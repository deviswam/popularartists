//
//  TopArtistsCollectionViewDataSource.swift
//  PopularArtists
//
//  Created by Waheed Malik on 29/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//


import UIKit

class TopArtistsCollectionViewDataSource: NSObject, UICollectionViewDataSource {
    var viewModel: TopArtistsViewModel?

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let viewModel = viewModel {
            return viewModel.numberOfArtists()
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArtistCollectionViewCell", for: indexPath)
        if let artistCell = cell as? ArtistCollectionViewCell, let artistVM = viewModel?.artist(at: indexPath.item) {
            artistCell.configCell(with: artistVM)
            loadPhoto(withId: artistVM.artist.id, photoUrl: artistVM.artist.imageUrl, onCell: artistCell)
        }
        return cell
    }

    func loadPhoto(withId id: UUID, photoUrl: URL?, onCell cell:ArtistCollectionViewCell) {
        guard let photoUrl = photoUrl else { return }
        viewModel?.getPhoto(forId: id, withUrl: photoUrl, { (result: Result<UIImage>) in
            if case let Result.success(image) = result {
                cell.setArtistImage(image: image)
            }
        })
    }
}

