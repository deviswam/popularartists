//
//  Result.swift
//  PopularArtists
//
//  Created by Waheed Malik on 28/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

enum Result<T> {
    case success(T)
    case failure(Error)
}
