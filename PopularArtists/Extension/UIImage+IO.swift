//
//  UIImage+IO.swift
//  PopularArtists
//
//  Created by Waheed Malik on 30/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

extension UIImage {
    
    static func saveImage(_ image: UIImage, withId id: UUID){
        createDirectoryIfNeeded()
        let fileManager = FileManager.default
        let imagePath = (self.getDirectoryPath() as NSString).appendingPathComponent(id.uuidString)
        let imageData = UIImageJPEGRepresentation(image, 0.5)
        fileManager.createFile(atPath: imagePath, contents: imageData, attributes: nil)
    }
    
    static func loadImage(forId id: UUID) -> UIImage? {
        createDirectoryIfNeeded()
        let fileManager = FileManager.default
        let imagePath = (self.getDirectoryPath() as NSString).appendingPathComponent(id.uuidString)
        if fileManager.fileExists(atPath: imagePath){
            return UIImage(contentsOfFile: imagePath)
        }
        return nil
    }
    
    private static func createDirectoryIfNeeded(){
        let fileManager = FileManager.default
        let path = getDirectoryPath()
        if !fileManager.fileExists(atPath: path) {
            try? fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        }
    }
    
    private static func getDirectoryPath() -> String {
        guard let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [NSString]).first?.strings(byAppendingPaths: ["Photos"]).first else {
            return ""
        }
        return path
    }
}
