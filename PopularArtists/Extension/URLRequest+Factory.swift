//
//  URLRequest+Factory.swift
//  PopularArtists
//
//  Created by Waheed Malik on 29/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

extension URLRequest {
    static func UrlRequestGET(url: URL, with parameters:[String: String] = [:]) -> URLRequest? {
        return URLRequest.UrlRequestGET(url: url.absoluteString, with: parameters)
    }
    
    static func UrlRequestGET(url: String, with parameters:[String: String] = [:]) -> URLRequest? {
        // create url
        var urlComponents = URLComponents(string: url)
        
        // append custom parameters
        var queryItems = [URLQueryItem]()
        for (key, value) in parameters {
            let query = URLQueryItem(name: key, value: value)
            queryItems.append(query)
        }
        
        urlComponents?.queryItems = queryItems
        guard let url = urlComponents?.url else { return nil }
        
        // create request object
        let request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 15)
        
        return request
    }
}
