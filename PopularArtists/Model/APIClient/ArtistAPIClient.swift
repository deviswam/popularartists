//
//  LastFmAPIClient.swift
//  PopularArtists
//
//  Created by Waheed Malik on 28/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

enum ArtistAPIClientError: Error {
    case httpError
    case invalidDataError
    case dataSerializationError
}

protocol ArtistAPIClient {
    func fetchTopArtists(completionHandler: @escaping (_ result: Result<JSONArray>) -> Void)
    func fetchTopAlbums(of artistName: String, completionHandler: @escaping (_ result: Result<JSONArray>) -> Void)
    func fetchPhoto(with photoUrl: URL, _ completionHandler: @escaping (Result<UIImage>) -> Void)
}

class LastFmAPIClient: ArtistAPIClient {
    // MARK: PRIVATE VARIABLES
    private let BASE_URL = "http://ws.audioscrobbler.com/2.0/"
    private let commonRequestParams = ["api_key": "c2c32ecdc9daf333f82c0ce79adc4297", "format": "json"]
    private let urlSession: URLSession!
    
    // MARK: INITIALIZER
    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }
    
    // MARK: PUBLIC METHODS
    func fetchPhoto(with photoUrl: URL, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        guard let urlRequest = URLRequest.UrlRequestGET(url: photoUrl) else {
            return
        }
        
        var result: Result<UIImage>!
        performRequest(with: urlRequest) { (reqResult: Result<Data>) in
            switch reqResult {
            case .success(let photoData):
                if let photo = UIImage(data: photoData) {
                    result = .success(photo)
                } else {
                    result = .failure(ArtistAPIClientError.invalidDataError)
                }
            case .failure(let error):
                result = .failure(error)
            }
            completionHandler(result)
        }
    }
    
    func fetchTopArtists(completionHandler: @escaping (_ result: Result<JSONArray>) -> Void) {
        var requestParams = ["method": "chart.gettopartists"]
        requestParams = requestParams.merging(commonRequestParams) {(key, _) in key}
        
        guard let urlRequest = URLRequest.UrlRequestGET(url: BASE_URL, with: requestParams) else {
            return
        }
        getTopArtists(with: urlRequest) { (result: Result<JSONArray>) in
            completionHandler(result)
        }
    }
    
    func fetchTopAlbums(of artistName: String, completionHandler: @escaping (_ result: Result<JSONArray>) -> Void) {
        var requestParams = ["method": "artist.gettopalbums",
                             "artist": artistName]
        requestParams = requestParams.merging(commonRequestParams) {(key, _) in key}
        
        guard let urlRequest = URLRequest.UrlRequestGET(url: BASE_URL, with: requestParams) else {
            return
        }
        getTopAlbums(with: urlRequest) { (result: Result<JSONArray>) in
            completionHandler(result)
        }
    }
    
    // MARK: PRIVATE HELPER METHODS
    private func getTopArtists(with urlRequest: URLRequest, completionHandler: @escaping (_ result: Result<JSONArray>) -> Void) {
        var result: Result<JSONArray>!
        
        performRequest(with: urlRequest) { (reqResult: Result<Data>) in
            switch reqResult {
            case .success(let artistsData):
                let artistsJSONResult = JSONDictionaryFactory.dictionary(data: artistsData)
                switch artistsJSONResult {
                    case .success(let artistsDictionary):
                        if let artistsDictionary = artistsDictionary["artists"] as? JSONDictionary,
                            let artistsArray = artistsDictionary["artist"] as? JSONArray {
                            result = .success(artistsArray)
                        } else {
                            result = .failure(ArtistAPIClientError.invalidDataError)
                        }
                    case .failure(let error):
                        result = .failure(error)
                }
            case .failure(let error):
                result = .failure(error)
            }
            completionHandler(result)
        }
    }
    
    private func getTopAlbums(with urlRequest: URLRequest, completionHandler: @escaping (_ result: Result<JSONArray>) -> Void) {
        var result: Result<JSONArray>!
        
        performRequest(with: urlRequest) { (reqResult: Result<Data>) in
            switch reqResult {
            case .success(let albumsData):
                let albumsJSONResult = JSONDictionaryFactory.dictionary(data: albumsData)
                switch albumsJSONResult {
                case .success(let albumsDictionary):
                    if let topAlbumsDictionary = albumsDictionary["topalbums"] as? JSONDictionary,
                        let albumsArray = topAlbumsDictionary["album"] as? JSONArray {
                        result = .success(albumsArray)
                    } else {
                        result = .failure(ArtistAPIClientError.invalidDataError)
                    }
                case .failure(let error):
                    result = .failure(error)
                }
            case .failure(let error):
                result = .failure(error)
            }
            completionHandler(result)
        }
    }
    
    private func performRequest(with urlRequest: URLRequest, completionHandler: @escaping (_ result: Result<Data>) -> Void) {
#if DEBUG_HTTP
        print("*** Request URL: \(urlRequest.description)")
#endif
        let dataTask = urlSession.dataTask(with: urlRequest) { (data, urlResponse, error) in
            var result: Result<Data>!
            
            if error != nil {
                result = .failure(ArtistAPIClientError.httpError)
            } else if let data = data {
                result = .success(data)
            }
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
        dataTask.resume()
    }
}
