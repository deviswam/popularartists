//
//  CoreDataStack.swift
//  PopularArtists
//
//  Created by Waheed Malik on 28/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack {
    let managedObjectModelName: String
    init(modelName: String) {
        managedObjectModelName = modelName
    }
    
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: managedObjectModelName)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    lazy var mainContext : NSManagedObjectContext = {
        return persistentContainer.viewContext
    }()
    
    lazy var backgroundContext : NSManagedObjectContext = {
        let backgroundContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        backgroundContext.parent = mainContext
        return backgroundContext
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        backgroundContext.perform {
            do {
                if self.backgroundContext.hasChanges {
                    try self.backgroundContext.save()
                }
            } catch {
                let saveError = error as NSError
                print("Unable to Save Changes of Private Managed Object Context")
                print("\(saveError), \(saveError.localizedDescription)")
            }
            
            self.mainContext.perform {
                do {
                    if self.mainContext.hasChanges {
                        try self.mainContext.save()
                    }
                } catch {
                    let saveError = error as NSError
                    print("Unable to Save Changes of Managed Object Context")
                    print("\(saveError), \(saveError.localizedDescription)")
                }
            }
        }
    }
}
