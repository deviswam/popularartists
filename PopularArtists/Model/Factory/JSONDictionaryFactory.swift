//
//  JSONDictionaryFactory.swift
//  PopularArtists
//
//  Created by Waheed Malik on 30/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

enum JSONDictionaryFactoryError : Error {
    case invalidJSONRootNode
    case dataSerializationError
}

class JSONDictionaryFactory {
    static func dictionary(data: Data) -> Result<JSONDictionary> {
        do {
            // Check for JSON Validation.
            if let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? JSONDictionary {
                return .success(dictionary)
            }
            return .failure(JSONDictionaryFactoryError.invalidJSONRootNode)
        } catch {
            return .failure(JSONDictionaryFactoryError.dataSerializationError)
        }
    }
}
