//
//  ModelFactory.swift
//  PopularArtists
//
//  Created by Waheed Malik on 28/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

import Foundation
import CoreData

enum ModelFactoryError : Error {
    case invalidJSONData
    case createOperationFailed
    case fetchOperationFailed
}

class ModelFactory {
    func createModelObjects<T:NSManagedObject>(jsonObject: AnyObject, inContext context: NSManagedObjectContext) -> Result<[T]> where T:ObjectMapper {
        guard let jsonArray = jsonObject as? [[String:AnyObject]] else {
            return .failure(ModelFactoryError.invalidJSONData)
        }
        
        var finalList = [T]()
        for jsonObjDictionary in jsonArray {
            var modelObj : T!
            context.performAndWait({
                modelObj = NSEntityDescription.insertNewObject(forEntityName: String(describing: T.self), into:context) as! T
                do {
                    try modelObj.mapPropertiesWithJSONDictionary(jsonObjDictionary)
                    finalList.append(modelObj)
                } catch {
                    context.delete(modelObj)
                }
            })
        }
        
        // If not able to parse any of the model dictionary means model json has been changed and not what we expect.
        if finalList.count == 0 && jsonArray.count > 0 {
            return .failure(ModelFactoryError.createOperationFailed)
        }
        return .success(finalList)
    }
    
    func fetchModelObjects<T:NSManagedObject>(withPredicate predicate: NSPredicate? = nil, fromContext context: NSManagedObjectContext) -> Result<[T]> where T:ObjectMapper {
        // Create Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: T.self))
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        fetchRequest.predicate = predicate
        
        var finalList = [T]()
        do {
            // Execute Fetch Request
            let records = try context.fetch(fetchRequest)
            if let records = records as? [T] {
                finalList = records
            }
        } catch {
            print("Unable to fetch managed objects for entity \(String(describing: T.self)).")
            return .failure(ModelFactoryError.fetchOperationFailed)
        }
        return .success(finalList)
    }
}
