//
//  ArtistManager.swift
//  PopularArtists
//
//  Created by Waheed Malik on 28/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

protocol ArtistManager {
    // gets list of top artists
    func loadTopArtists(completionHandler: @escaping (_ result: Result<[Artist]>) -> Void)
    // gets albums of an artist
    func loadTopAlbums(of artist: Artist, completionHandler: @escaping (_ result: Result<[Album]>) -> Void)
}

class ArtistManagerImpl: ArtistManager {
    // MARK: PRIVATE VARIABLES
    private let artistAPIClient: ArtistAPIClient
    private let modelFactory: ModelFactory
    private let coreDataStack: CoreDataStack
    
    // MARK: INITIALIZER
    init(artistAPIClient: ArtistAPIClient, modelFactory: ModelFactory, coreDataStack: CoreDataStack) {
        self.artistAPIClient = artistAPIClient
        self.modelFactory = modelFactory
        self.coreDataStack = coreDataStack
    }
    
    func loadTopArtists(completionHandler: @escaping (_ result: Result<[Artist]>) -> Void) {
        var artistsResult: Result<[Artist]>!
        
        // Check from db first if artist records exist
        let fetchModelObjsResult : Result<[Artist]> = self.modelFactory.fetchModelObjects(fromContext: self.coreDataStack.backgroundContext)
        
        if case let .success(artists) = fetchModelObjsResult, artists.count != 0 {
            completionHandler(.success(artists))
            return
        }
        
        // Otherwise download the artists and store them in db
        artistAPIClient.fetchTopArtists { [unowned self] (result: Result<JSONArray>) in
            switch result {
            case .success(let artistsJsonArray):
                let createModelObjsResult : Result<[Artist]> = self.modelFactory.createModelObjects(jsonObject: artistsJsonArray as AnyObject, inContext: self.coreDataStack.backgroundContext)
                switch createModelObjsResult {
                case .success(var artists):
                    self.coreDataStack.saveContext()
                    artists.sort { $0.name < $1.name }
                    artistsResult = .success(artists)
                case .failure(let modelFactoryError):
                    artistsResult =  .failure(modelFactoryError)
                }
                
            case .failure(let apiError):
                artistsResult = .failure(apiError)
            }
            
            completionHandler(artistsResult)
        }
    }
    
    func loadTopAlbums(of artist: Artist, completionHandler: @escaping (_ result: Result<[Album]>) -> Void) {
        var albumsResult: Result<[Album]>!
        
        // Check from db first if artist's album records exist
        let fetchModelObjsResult : Result<[Album]> = self.modelFactory.fetchModelObjects(withPredicate: NSPredicate(format: "artist.id == %@", artist.id as CVarArg), fromContext: self.coreDataStack.backgroundContext)

        if case let .success(albums) = fetchModelObjsResult, albums.count != 0 {
            completionHandler(.success(albums))
            return
        }
        
        // Otherwise download the albums and store them in db
        artistAPIClient.fetchTopAlbums(of: artist.name) { [unowned self] (result: Result<JSONArray>) in
            switch result {
            case .success(let albumsJsonArray):
                let createModelObjsResult : Result<[Album]> = self.modelFactory.createModelObjects(jsonObject: albumsJsonArray as AnyObject, inContext: self.coreDataStack.backgroundContext)
                switch createModelObjsResult {
                case .success(var albums):
                    artist.albums = NSSet(array: albums)
                    self.coreDataStack.saveContext()
                    albums.sort { $0.name < $1.name }
                    albumsResult = .success(albums)
                case .failure(let modelFactoryError):
                    albumsResult =  .failure(modelFactoryError)
                }
                
            case .failure(let apiError):
                albumsResult = .failure(apiError)
            }
            
            completionHandler(albumsResult)
        }
    }
}
