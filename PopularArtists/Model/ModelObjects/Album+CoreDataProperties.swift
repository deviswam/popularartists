//
//  Album+CoreDataProperties.swift
//  PopularArtists
//
//  Created by Waheed Malik on 01/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//
//

import Foundation
import CoreData


extension Album {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Album> {
        return NSFetchRequest<Album>(entityName: "Album")
    }

    @NSManaged public var artist: Artist?

}
