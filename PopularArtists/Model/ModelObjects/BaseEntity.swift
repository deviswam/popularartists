//
//  BaseEntity+CoreDataClass.swift
//  PopularArtists
//
//  Created by Waheed Malik on 01/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//
//

import Foundation
import CoreData

@objc(BaseEntity)
public class BaseEntity: NSManagedObject, ObjectMapper {
    func mapPropertiesWithJSONDictionary(_ dictionary: JSONDictionary) throws {
        guard let uuidStr = dictionary["mbid"] as? String, let _id = UUID(uuidString: uuidStr),
            let _name = dictionary["name"] as? String, !_name.isEmpty else {
                throw ModelFactoryError.invalidJSONData;
        }
        
        var _imageUrl: URL?
        if var imagesArray = dictionary["image"] as? [[String: AnyObject]] {
            // filter the imagesArray to take out medium image Dictionary
            imagesArray = imagesArray.filter({ imageDictionary -> Bool in
                if let imageSize = imageDictionary["size"] as? String {
                    return imageSize == "large"
                }
                return false
            })
            
            if let imageUrlStr = imagesArray.first?["#text"] as? String {
                _imageUrl = URL(string: imageUrlStr)
            }
        }
        
        id = _id
        name = _name
        imageUrl = _imageUrl!
    }
    
    public override func awakeFromInsert() {
        super.awakeFromInsert()
        id = UUID.init()
        name = ""
    }
}
