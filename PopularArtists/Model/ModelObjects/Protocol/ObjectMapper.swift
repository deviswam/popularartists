//
//  ObjectMapper.swift
//  PopularArtists
//
//  Created by Waheed Malik on 27/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

protocol ObjectMapper {
    func mapPropertiesWithJSONDictionary(_ dictionary: JSONDictionary) throws
}
