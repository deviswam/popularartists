//
//  SharedComponentsDir.swift
//  PopularArtists
//
//  Created by Waheed Malik on 29/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

class SharedComponentsDir {
    static let artistManager: ArtistManager = {
        let artistManager = ArtistManagerImpl(artistAPIClient: SharedComponentsDir.apiClient, modelFactory: SharedComponentsDir.modelFactory, coreDataStack: SharedComponentsDir.coreDataStack)
        return artistManager
    }()
    
    static let imageStore: ImageStore = {
        let imageStore = ImageStore(artistAPIClient: SharedComponentsDir.apiClient)
        return imageStore
    }()
    
    static let apiClient: ArtistAPIClient = {
        return LastFmAPIClient()
    }()
    
    static let modelFactory: ModelFactory = {
        return ModelFactory()
    }()
    
    static let coreDataStack: CoreDataStack = {
        return CoreDataStack(modelName: "PopularArtists")
    }()
}
