//
//  ImageStore.swift
//  PopularArtists
//
//  Created by Waheed Malik on 30/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

enum ImageStoreError: Error {
    case unableToDownload
}

class ImageStore {
    // MARK: PRIVATE VARIABLES
    private let artistAPIClient: ArtistAPIClient
    
    // MARK: INITIALIZER
    init(artistAPIClient: ArtistAPIClient) {
        self.artistAPIClient = artistAPIClient
    }
    
    func getImage(forId id: UUID, withUrl imageUrl: URL, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        if let image = UIImage.loadImage(forId: id) {
            completionHandler(.success(image))
        } else {
            var result: Result<UIImage>!
            artistAPIClient.fetchPhoto(with: imageUrl) { (apiResult: Result<UIImage>) in
                switch apiResult {
                case .success(let image):
                    UIImage.saveImage(image, withId: id)
                    result = .success(image)
                case .failure(let error):
                    print("Unable to download image. Error: \(error)")
                    result = .failure(ImageStoreError.unableToDownload)
                }
                completionHandler(result)
            }
        }
    }
}
