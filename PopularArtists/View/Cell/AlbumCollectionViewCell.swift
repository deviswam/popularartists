//
//  AlbumCollectionViewCell.swift
//  PopularArtists
//
//  Created by Waheed Malik on 01/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class AlbumCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var albumNameLabel: UILabel!
    @IBOutlet weak var albumCoverImageView: UIImageView!
    
    func configCell(with albumViewModel: AlbumViewModel) {
        self.albumNameLabel.text = albumViewModel.name
    }
    
    func setAlbumImage(image: UIImage) {
        self.albumCoverImageView.image = image
    }
    
    override func prepareForReuse() {
        self.albumCoverImageView.image = UIImage(named: "album_placeholder")
    }
}
