//
//  ArtistCollectionViewCell.swift
//  PopularArtists
//
//  Created by Waheed Malik on 30/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class ArtistCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var artistImageView: UIImageView!
    @IBOutlet weak var artistNameLabel: UILabel!
    
    func configCell(with artistViewModel: ArtistViewModel) {
        self.artistNameLabel.text = artistViewModel.name
    }
    
    func setArtistImage(image: UIImage) {
        self.artistImageView.image = image
    }
    
    override func prepareForReuse() {
        self.artistImageView.image = UIImage(named: "artist_placeholder")
    }
}
