//
//  ArtistDetailViewController.swift
//  PopularArtists
//
//  Created by Waheed Malik on 01/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class ArtistDetailViewController: UIViewController {

    @IBOutlet weak var artistImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var albumsCollectionView: UICollectionView!
    
    var viewModel: ArtistDetailViewModel?
    var albumsCollectionViewDataSource: AlbumsCollectionViewDataSource?
    var albumsCollectionViewDelegate: AlbumsCollectionViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        albumsCollectionView.dataSource = albumsCollectionViewDataSource
        albumsCollectionView.delegate = albumsCollectionViewDelegate
        
        self.titleLabel.text = viewModel?.title()
        viewModel?.getArtistPhoto(completion: { [weak self] (artistImage) in
            self?.artistImageView.image = artistImage
        })
        
        // ask for top albums for selected artist.
        viewModel?.getTopAlbumsOfSelectedArtist()
    }
}

extension ArtistDetailViewController: ArtistDetailViewModelViewDelegate {
    func albumsLoaded(with result: Result<Void>) {
        switch result {
        case .success:
            self.albumsCollectionView.reloadData()
        case .failure:
            print("Info (Inform user): Unable to fetch albums list")
        }
    }
}
