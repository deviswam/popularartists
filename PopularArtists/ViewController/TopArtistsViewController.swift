//
//  ViewController.swift
//  PopularArtists
//
//  Created by Waheed Malik on 27/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class TopArtistsViewController: UIViewController {

    @IBOutlet weak var topArtistsCollectionView: UICollectionView!
    
    var artistsCollectionViewDataSource: UICollectionViewDataSource?
    var artistsCollectionViewDelegate: UICollectionViewDelegate?
    
    var viewModel: TopArtistsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topArtistsCollectionView.dataSource = artistsCollectionViewDataSource
        topArtistsCollectionView.delegate = artistsCollectionViewDelegate
        
        // register artist cell
        registerArtistCell()
        
        // get all top artists
        viewModel?.getTopArtists()
    }
    
    //MARK: PRIVATE METHODS
    private func registerArtistCell() {
        let nib = UINib(nibName: "ArtistCollectionViewCell", bundle: Bundle.main)
        topArtistsCollectionView.register(nib, forCellWithReuseIdentifier: "ArtistCollectionViewCell")
    }
}

extension TopArtistsViewController: TopArtistsViewModelViewDelegate {
    func artistsLoaded(with result: Result<Void>) {
        switch result {
        case .success:
            self.topArtistsCollectionView.reloadData()
        case .failure:
            print("Info (Inform user): Unable to fetch matching artists list")
        }
    }
}

