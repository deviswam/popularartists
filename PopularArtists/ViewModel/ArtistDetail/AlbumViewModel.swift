//
//  AlbumViewModel.swift
//  PopularArtists
//
//  Created by Waheed Malik on 01/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

protocol AlbumViewModel {
    var album: Album { get }
    var name: String { get }
}

class AFAlbumViewModel: AlbumViewModel {
    var album: Album
    var name: String = ""
    init(album: Album) {
        self.album = album
        self.name = album.name
    }
}
