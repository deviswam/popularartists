//
//  ArtistDetailViewModel.swift
//  PopularArtists
//
//  Created by Waheed Malik on 01/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

// MARK:- VIEW DELEGATE PROTOCOL
protocol ArtistDetailViewModelViewDelegate: class {
    func albumsLoaded(with result: Result<Void>)
}

// MARK:- VIEWMODEL PROTOCOL
protocol ArtistDetailViewModel {
    func getTopAlbumsOfSelectedArtist()
    func numberOfAlbums() -> Int
    func album(at index: Int) -> AlbumViewModel?
    func title() -> String
    func getPhoto(forId id: UUID, withUrl imageUrl: URL, _ completionHandler: @escaping (Result<UIImage>) -> Void)
    func getArtistPhoto(completion: @escaping (UIImage) -> Void)
}

class ArtistDetailViewModelImpl: BaseViewModel, ArtistDetailViewModel {
    // MARK: PRIVATE VARIABLES
    private var selectedArtist: Artist
    private var artistAlbums: [Album]?
    private weak var viewDelegate: ArtistDetailViewModelViewDelegate!
    
    // MARK: INITIALIZER
    init(selectedArtist: Artist, artistManager: ArtistManager, imageStore: ImageStore, viewDelegate: ArtistDetailViewModelViewDelegate) {
        self.selectedArtist = selectedArtist
        self.viewDelegate = viewDelegate
        super.init(artistManager: artistManager, imageStore: imageStore)
    }
    
    // MARK: PUBLIC METHODS
    func getTopAlbumsOfSelectedArtist() {
        var vmResult: Result<Void>!
        artistManager.loadTopAlbums(of: self.selectedArtist) { [weak self] (result: Result<[Album]>) in
            switch result {
            case .success(let albums):
                self?.artistAlbums = albums
                vmResult = .success(())
            case .failure(let error):
                vmResult = .failure(error)
            }
            self?.viewDelegate.albumsLoaded(with: vmResult)
        }
    }
    
    override func getPhoto(forId id: UUID, withUrl imageUrl: URL, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        super.getPhoto(forId: id, withUrl: imageUrl) { result in
            completionHandler(result)
        }
    }
    
    func numberOfAlbums() -> Int {
        guard let albums = self.artistAlbums else {
            return 0
        }
        return albums.count
    }
    
    func album(at index: Int) -> AlbumViewModel? {
        if let albums = self.artistAlbums, index < albums.count {
            return AFAlbumViewModel(album:  albums[index])
        }
        return nil
    }
    
    func title() -> String {
        return "\(selectedArtist.name)'s Top Albums"
    }
    
    func getArtistPhoto(completion: @escaping (UIImage) -> Void) {
        if let photoUrl = self.selectedArtist.imageUrl {
            getPhoto(forId: self.selectedArtist.id, withUrl: photoUrl) { (result: Result<UIImage>) in
                if case let Result.success(image) = result {
                    completion(image)
                }
            }
        }
    }
}

