//
//  BaseViewModel.swift
//  PopularArtists
//
//  Created by Waheed Malik on 01/05/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

class BaseViewModel {
    // MARK: PRIVATE VARIABLES
    let artistManager: ArtistManager
    let imageStore: ImageStore
    
    init(artistManager: ArtistManager, imageStore: ImageStore) {
        self.artistManager = artistManager
        self.imageStore = imageStore
    }
    
    func getPhoto(forId id: UUID, withUrl imageUrl: URL, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        imageStore.getImage(forId: id, withUrl: imageUrl) { (result: Result<UIImage>) in
            completionHandler(result)
        }
    }
}
