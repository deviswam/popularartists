//
//  ArtistViewModel.swift
//  PopularArtists
//
//  Created by Waheed Malik on 29/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

protocol ArtistViewModel {
    var artist: Artist{ get }
    var name: String { get }
}

class ArtistViewModelImpl: ArtistViewModel {
    var artist: Artist
    var name: String = ""
    
    init(artist: Artist) {
        self.artist = artist
        self.name = artist.name
    }
}
