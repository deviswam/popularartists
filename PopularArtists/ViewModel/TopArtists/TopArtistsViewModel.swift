//
//  TopArtistsViewModel.swift
//  PopularArtists
//
//  Created by Waheed Malik on 28/04/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import UIKit

// MARK:- COORDINATOR DELEGATE PROTOCOL
protocol TopArtistsViewModelCoordinatorDelegate: class {
    func didSelect(artist: Artist)
}

// MARK:- VIEW DELEGATE PROTOCOL
protocol TopArtistsViewModelViewDelegate: class {
    func artistsLoaded(with result: Result<Void>)
}

// MARK:- VIEWMODEL PROTOCOL
protocol TopArtistsViewModel {
    func getTopArtists()
    func numberOfArtists() -> Int
    func artist(at index: Int) -> ArtistViewModel?
    func selectedArtist(at index: Int)
    func getPhoto(forId id: UUID, withUrl imageUrl: URL, _ completionHandler: @escaping (Result<UIImage>) -> Void)
}

class TopArtistsViewModelImpl: BaseViewModel, TopArtistsViewModel {
    // MARK: PRIVATE VARIABLES
    private weak var viewDelegate: TopArtistsViewModelViewDelegate!
    private weak var coordinatorDelegate: TopArtistsViewModelCoordinatorDelegate!
    private var artists: [Artist]?
    
    // MARK: INITIALIZER
    init(artistManager: ArtistManager, imageStore: ImageStore, viewDelegate: TopArtistsViewModelViewDelegate, coordinatorDelegate: TopArtistsViewModelCoordinatorDelegate) {
        super.init(artistManager: artistManager, imageStore: imageStore)
        self.viewDelegate = viewDelegate
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    // MARK: PUBLIC METHODS
    func getTopArtists() {
        var vmResult: Result<Void>!
        artistManager.loadTopArtists { [weak self] result in
            switch result {
            case .success(let artists):
                self?.artists = artists
                vmResult = .success(())
            case .failure(let error):
                vmResult = .failure(error)
            }
            self?.viewDelegate.artistsLoaded(with: vmResult)
        }
    }
    
    override func getPhoto(forId id: UUID, withUrl imageUrl: URL, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        super.getPhoto(forId: id, withUrl: imageUrl) { result in
            completionHandler(result)
        }
    }
    
    func numberOfArtists() -> Int {
        guard let artists = self.artists else {
            return 0
        }
        return artists.count
    }
    
    func artist(at index: Int) -> ArtistViewModel? {
        if let artists = self.artists, index < artists.count {
            return ArtistViewModelImpl(artist: artists[index])
        }
        return nil
    }
    
    func selectedArtist(at index: Int) {
        guard let artists = self.artists, index < artists.count else {
            return
        }
        self.coordinatorDelegate.didSelect(artist: artists[index])
    }
}
