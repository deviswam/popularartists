IMPORTANT NOTES
===============

1. The project fully implements the following user stories and has two screens.
    *  `Top Artists` screen that shows popular artists in collection view and `Top Albums` screen that shows the famous albums of selected artist. 
    * Data feeds (both artists and their albums) once downloaded are then stored in to database using Coredata for later use. Also, any images once downloaded for artist or for their albums are locally stored. When same data needs to be presented again, it is loaded from persistent storage (database and filesystem) and only the new data (json/ images) which is not presented locally will be fetched from the server. This can be seen in the console via `*** Request URL:` prefix which gives the idea as what data is being requested from the server API at any point in time.    
2. `iOS 11.0` onwards supported. 
3. Its a universal app and supports all major iPhone and iPad screens. 
4. The project has been developed on latest `XCode 9.3` with `Swift 4.1` without any warnings or errors.
5. `Lastfm` free API has been used to fetch famous artists and their top albums data. 
6. The app is following `MVVM-C (Model-View-ViewModel-Coordinator)` architecture pattern.
7. I preferred to use all the built-in native iOS technologies and not using third-party libraries or CocoaPods for this small project.
8. This project is built on best software engineering practices and code structure is easy to follow. To name a few best practices followed: `SOLID principles, design patterns and loosely coupled architecture`.

Note: This app has been developed and tested thoroughly and carefully, so nothing should go wrong but Incase such happens please inform recruitement agent so that issue can be addressed.

Many thanks for your time.

